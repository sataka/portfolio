import haravasto as h
import random as r
import time as t 

tila = {
    "kentta": None,
    "vuorot": None,
    "aloitus_aika": None,
    "lopetus_aika": None,
    "tulos": None,
    "miinat": None,
    "leveys": None,
    "korkeus": None,
    "ajankohta": None
}
        
def miinoita(kentta, vapaat, n): #Valmis
    """Asettaa kentällä N kpl miinoja satunnaisiin paikkoihin."""
    kp = r.sample(vapaat, n)
    for x, y in kp:
        kentta[y][x] = "x"
        
def numeroi(kentta): #Valmis
    """Laskee ruuduille kuinka monta miinaa niiden vieressä on ja lisää numeron listaan"""
    for y, lista in enumerate(kentta):
        for x, arvo in enumerate(lista):
            numero = 0
            if kentta[y][x] != "x":
                for i in range(3):
                    for j in range(3):
                        try:
                            if x + i -1 < 0 or y+ j -1 < 0 or (x + i - 1 == x and y+ j -1 == y): #Ruudut ulkopuolella
                                continue
                            else:
                                if kentta[y+ j -1][x + i -1] == "x": #Miina vieressä
                                    numero += 1
                        except IndexError:
                            pass
                kentta[y][x] = "{}".format(numero)
            
def kysy_kentta(): #Valmis
    """Kysyy pelaajalta kentän korkeuden ja leveyden, sekä miinojen määrän"""
    while True:
        try:
            korkeus = int(input("Anna Korkeus: "))
            leveys = int(input("Anna leveys: "))
            if leveys <= 0 or korkeus <= 0: #Mutta voi olla niin iso kun haluaa, oma vika jos kone ei kestä
                print("Ei nollaa tai alle kiitos...")
                continue
            else:
                while True:
                    miinat = int(input("Anna miinojen määrä: "))
                    if miinat <= 0:
                        print("Ei nollaa tai alle kiitos...")
                        continue
                    elif miinat > korkeus * leveys:
                        print("Liikaa miinoja kenttään nähden.")
                        continue
                    tila["miinat"] = miinat #Ajatuksena oli vaihtaa ettei ole returnia vaan suoraan sanakirjasta, mutta koodi meni rikki ja liika vaihdettavaa. Toimii näin.
                    tila["leveys"] = leveys
                    tila["korkeus"] = korkeus
                    return(korkeus, leveys, miinat)
        except ValueError:
            print("Anna syötteet kokonaislukuina!")
            
def luo_kentta(korkeus, leveys, miinojen_maara): #Valmis
    """Luo kentän johon miinat sijoitetaan miinoita funktion avulla """
    kentta = []
    for rivi in range(korkeus):
        kentta.append([])
        for sarake in range(leveys):
            kentta[-1].append(" ")
    tila["kentta"] = kentta
    jaljella = []
    for y in range(korkeus):
        for x in range(leveys):
            jaljella.append((x, y))
    miinoita(tila["kentta"], jaljella, miinojen_maara)
    numeroi(tila["kentta"])

def kasittele_hiiri(hiiri_x, hiiri_y, hiiren_painike, painetut_muokkausnappaimet): #Valmis
    try:
        painike = {"vasen": h.HIIRI_VASEN, "oikea": h.HIIRI_OIKEA}
        x = int(int(hiiri_x) / 40)
        y = int(int(hiiri_y)/ 40)
        if hiiren_painike == painike["vasen"]:
            if nakyva_kentta[y][x] != " ": #Ei aukaise lippuja tai lisää vuoroja kun mitään ei avata
                pass
            else:
                if tila["vuorot"] == 0:
                    tila["aloitus_aika"] = t.time()
                    tila["ajankohta"] = t.strftime("%d.%m.%Y - klo %H.%M.%S", t.localtime())
                tila["vuorot"] += 1
                tulvataytto(tila["kentta"], x, y)
        elif hiiren_painike == painike["oikea"]:
            if nakyva_kentta[y][x] == " ":
                if tila["vuorot"] == 0:
                    tila["aloitus_aika"] = t.time()
                    tila["ajankohta"] = t.strftime("%d.%m.%Y - klo %H.%M.%S", t.localtime())
                tila["vuorot"] += 1 #Pitäisikö poistaa lippu vuorot? Tai tehdä erilliseksi "normi" vuoroista. Nah
                nakyva_kentta[y][x] = "f"
            elif nakyva_kentta[y][x] == "f":
                if tila["vuorot"] == 0:
                    tila["aloitus_aika"] = t.time()
                    tila["ajankohta"] = t.strftime("%d.%m.%Y - klo %H.%M.%S", t.localtime())
                tila["vuorot"] += 1
                nakyva_kentta[y][x] = " "
        lippu_tyhja = 0
        for rivi in nakyva_kentta:
            lippu_tyhja += (rivi.count(" ") + rivi.count("f"))
        if tila["miinat"] == lippu_tyhja: #Ei voita automaattisesti jos kenttä on 100% miinoja, mutta sen siitä saa kun ei pelaa kunnolla
            tila["tulos"] = "Voitto"
            tila["lopetus_aika"] = t.time()
            tallenna_tilastot("miinaharava_tulokset.txt", tee_lista())
            h.lopeta()
            print("\nVoitit pelin!\n")
    except IndexError: #Ruudun ulkopuolelle painavat, onnistuu vain jos venyttää ruutua sen luonnin jälkeen, tai fullscreen.
        print("Mene vankilaan kulkematta lähtöruudun kautta.")
        
def tulvataytto(kentta, aloitusx, aloitusy): #Valmis
    """Avaa ruutuja sen mukaan mitä ruutua painetaan: Tyhjä aiheuttaa tulvatäytön, Miina lopettaa pelin, Numero aukaisee itse ruudun"""
    x = int(aloitusx)
    y = int(aloitusy)
    lista = [(x, y)]
    while not lista == []:
        koordinaatti = lista.pop()
        x, y = koordinaatti
        if kentta[y][x] == "x": #"Nytten kosahti" - Elmeri
            nakyva_kentta[y][x] = kentta[y][x] #Sairaan nopee close, joten turha. Maybe add delay ja teksti Hävisit pelin itse ikkunaan ei komento riville
            tila["tulos"] = "Häviö"
            tila["lopetus_aika"] = t.time()
            tallenna_tilastot("miinaharava_tulokset.txt", tee_lista())
            h.lopeta()
            print("\nHävisit pelin!\n")
        elif kentta[y][x] == "0" and nakyva_kentta[y][x] != "0":
            nakyva_kentta[y][x] = "0"
            for i in range(3):
                for j in range(3):
                    try:
                        if x + i -1 < 0 or y+ j -1 < 0 or (x + i - 1 == x and y+ j -1 == y): #Ulkopuoliset + Itse ruutu ei käsiteltäviksi
                            continue
                        else:
                            if nakyva_kentta[y+ j -1][x + i -1] == "f": #Ei poisteta lippuja
                                pass
                            elif kentta[y+ j -1][x + i -1] == "0": #Nollat listaan käsiteltäviksi
                                lista.append((x + i -1, y+ j -1))
                            else:
                                nakyva_kentta[y+ j -1][x + i -1] = kentta[y+ j -1][x + i -1] #Numerot Näkyviin
                    except IndexError:
                        pass
        else:
            nakyva_kentta[y][x] = kentta[y][x]
            
def piirra_kentta(): #Valmis
    h.tyhjaa_ikkuna() #(pyyhkii edellisen kierroksen grafiikat pois)
    h.piirra_tausta() #(asettaa ikkunan taustavÃ¤rin)
    h.aloita_ruutujen_piirto() #(kutsutaan ennen varsinaisen ruudukon piirtoa)
    for indeksiy, lista in enumerate(nakyva_kentta):
        for indeksix, arvo in enumerate(lista):
            y = indeksiy * 40
            x = indeksix * 40
            h.lisaa_piirrettava_ruutu(arvo, x, y)
    h.piirra_ruudut() #(piirtÃ¤Ã¤ kaikki aloituksen jÃ¤lkeen lisÃ¤tyt ruudut)

def main(kentta): #Valmis
    """Lataa pelin grafiikat, luo peli-ikkunan ja asettaa siihen piirtokäsittelijän."""
    h.lataa_kuvat("spritet")
    h.luo_ikkuna(len(kentta[0]) * 40, len(kentta) * 40)
    h.aseta_piirto_kasittelija(piirra_kentta)
    h.aseta_hiiri_kasittelija(kasittele_hiiri)
    h.aloita()
  
def nayta_tilastot(tiedosto): #Valmis
    """Näyttää kaikkien pelien tilastot"""
    try:
        with open(tiedosto) as lahde:
            for rivi in lahde.readlines():
                lista = rivi.strip("\n").split(",")
                print("Pelin ajankohta: {},  kesto: {} min,  vuorojen määrä: {},  lopputulos: {},  kentän koko {}x{} ruutua,  miinojen määrä: {}.".format(lista[0], lista[1], lista[2], lista[3], lista[4], lista[5], lista[6]))
    except IOError:
        pass
        
def tee_lista(): #Valmis
    """Tekee listan joka soveltuu funktioon tallenna_tilastot"""
    lista = []
    kesto =  int((tila["lopetus_aika"] - tila["aloitus_aika"]) / 60)
    monikko = (tila["ajankohta"], kesto, tila["vuorot"], tila["tulos"], tila["korkeus"], tila["leveys"], tila["miinat"])
    lista.append(monikko)
    return(lista)
    
def tallenna_tilastot(tiedosto, lista): #Valmis
    """Lisää pelin tilaston tilasto tiedostoon."""
    try:
        with open(tiedosto, "a") as kohde:
            for arvo in lista:
                kohde.write("{},{},{},{},{},{},{}\n".format(arvo[0], arvo[1], arvo[2], arvo[3], arvo[4], arvo[5], arvo[6]))
    except(IOError):
        print("Kohdetiedostoa ei voitu avata. Tallennus epäonnistui")
    
if __name__ == "__main__":
    while True:
        toiminto = input("Valitse toiminto:\nUusi peli: u\nLopeta: l\nTilastot: t\n")
        if toiminto.lower() == "l":
            break
        elif toiminto.lower() == "t":
            nayta_tilastot("miinaharava_tulokset.txt")
        elif toiminto.lower() == "u":
            korkeus, leveys, miinat = kysy_kentta()
            luo_kentta(korkeus, leveys, miinat)
            nakyva_kentta = []
            tila["vuorot"] = 0
            for rivi in range(korkeus):
                nakyva_kentta.append([])
                for sarake in range(leveys):
                    nakyva_kentta[-1].append(" ")
            main(nakyva_kentta)
        else:
            print("Syöte ei vastaa mitään vaihtoehdoista.")