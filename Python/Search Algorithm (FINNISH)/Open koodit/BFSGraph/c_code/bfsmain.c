#include <stdio.h>
#include "bfsgraph.h"

int main(){
	bfsgraph g;
	init_graph(&g,8);
	int colors[9];

	add_edge(&g,1,2);
	add_edge(&g,1,4);
	add_edge(&g,1,5);

	add_edge(&g,2,3);
	add_edge(&g,3,4);
	add_edge(&g,5,6);

	add_edge(&g,6,7);
	add_edge(&g,6,8);

	// The other graph
	// add_edge(&g,2,4);

	if( bicolor(&g,colors) == 0){
        printf("Coloring is not possible!\n");
	}
	else {
        int i;
        printf("Coloring is possible:\n");
        for(i=1; i <= 8; i++){
            printf("Node[%d] = ",i);
            colors[i]==RED?printf("RED\n"):printf("BLUE\n");
        }
	}

	delete_graph(&g,8);

	return 0;
}
