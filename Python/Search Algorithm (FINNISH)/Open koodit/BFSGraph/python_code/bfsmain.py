# Test function for BFS
from bfsgraph import *

def main():
	g = BFSGraph(8)
	
	add_edge(g,1,2)
	add_edge(g,1,4)
	add_edge(g,1,5)
	
	add_edge(g,2,3)
	add_edge(g,3,4)
	add_edge(g,5,6)
	
	add_edge(g,6,7)
	add_edge(g,6,8)
	
	# The other graph
	#add_edge(g,2,4)

	colors = bicolor(g)
	if colors == None:
		print("Coloring is not possible")
	else:
		print("Coloring is possible:")
		print(colors)

main()