# Python code to perform Breadth-first search
# in a non-directed graph
import math
from collections import deque

# Colors and infinity
WHITE = 0
GREY = 1
BLACK = 2
INF = float('inf')

#Colors for bicoloring
UNCOLORED = -1
RED = 0
BLUE = 1

# We assume that nodes numbered from 1 to n

# class for edges in adjacency list
class EdgeNode:
	def __init__(self,nde,nxt=None):
		self.node = nde

# graph class for breadth-forst search 
class BFSGraph:
	
	def __init__(self,nVerts):
		self.nVertices = nVerts
		self.adj_list = {}
		self.vertices = []
		
		for x in range(1,nVerts+1):
			self.adj_list[x] = []
			self.vertices.append(x)
			
		self.color = {}
		for x in range(1,nVerts+1):
			self.color[x] = WHITE
			
		self.dist = {}
		for x in range(1,nVerts+1):
			self.dist[x] = INF
			
		self.pred = {}
		for x in range(1,nVerts+1):
			self.pred[x] = None

		
# adds edge (x,y)		
def add_edge(g,x,y):	
	g.adj_list[x].append(EdgeNode(y))
	g.adj_list[y].append(EdgeNode(x))	

	
'''
BFS(G,s)
1.   for each u in V
2.      color[u] = WHITE
3.      d[u] = INF
4.      p[u] = NIL
5.   d[s] = 0
6.   color[s] = GRAY
7.   Q = EMPTY
8.   ENQUEUE(Q,s)
9.   while Q != EMPTY
10.    u = DEQUEUE(Q)
11.	   for each v in Adj[u]
12.        if color[v] == WHITE
13.           color[v] = GRAY
14.           d[v] = d[u]+1
15.           p[v] = u
16.	          ENQUEUE(Q,v)   
17.    color[u] = BLACK
18.  return
'''

# Actual breadth-first search from node s
def bfsearch(g,s):
	queue = deque([])
	
	for i in g.vertices:
		g.color[i] = WHITE
		g.dist[i] = INF
		g.pred[i] = 0
		
	g.dist[s] = 0
	g.color[s] = GREY
	queue.append(s)
	
	while len(queue)!=0:
		u = queue.popleft()
		
		for edge in g.adj_list[u]:
			v = edge.node
			if g.color[v] == WHITE:
				g.color[v] = GREY
				g.dist[v] = g.dist[u] + 1
				g.pred[v] = u
				queue.append(v)
		
		# DEBUG INFO:
		#print(u, " processed, dist =  ",g.dist[u])
		
		g.color[u] = BLACK

def bicolor(g):
	queue = deque([])
	colortable = {}
	
	for x in g.vertices:
		colortable[x] = UNCOLORED
		
	for i in g.vertices:
		g.color[i] = WHITE
		g.dist[i] = INF
		g.pred[i] = 0

	# One has to process all vertices
	for s in g.vertices:
		if g.color[s] == WHITE:
			g.dist[s] = 0
			g.color[s] = GREY
			queue.append(s)
			colortable[s] = RED
			
			while len(queue)!=0:
				u = queue.popleft()
		
				for edge in g.adj_list[u]:
					v = edge.node
					if colortable[v] == colortable[u]:
						return None
					else:
						colortable[v] = (colortable[u]+1)%2
						
					if g.color[v] == WHITE:
						g.color[v] = GREY
						g.dist[v] = g.dist[u] + 1
						g.pred[v] = u
						queue.append(v)
		
				# DEBUG INFO:
				print(u, " processed, dist =  ",g.dist[u])
		
				g.color[u] = BLACK

			
	# coloring was possible
	return colortable
		
def print_path(g,u):
	if g.pred[u] != 0:
		print_path(g,g.pred[u])
		print(u)

# Returns True if path from s to d
# False otherwise		
def ispath(g,s,d):
	bfsearch(g,s)
	if g.dist[d] == INF:
		return False
		
	return True

# Returns the path from s to d
# Empty list if no path	
def getpath(g,s,d):
	path = []
	bfsearch(g,s)
	u = d
	if g.dist[d] != INF:
		while g.pred[u] != 0:
			path.append(u)
			u = g.pred[u]
		path.append(s)	
	path.reverse()	
	return path