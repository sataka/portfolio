#Santtu Käpylä - 2523734

import math
from collections import deque
import time

#Arvoja väreille leveyshakuun
VALKOINEN = 0
HARMAA = 1
MUSTA = 2

class verkko:           #verkko luokka

    def __init__(self, kaupunkien_maara, reittien_maara):
        """Initalisoi luokan parametreilla kaupunkien_maara, teitten_maara ja maaranpaa"""
        self.kaupunkien_maara = kaupunkien_maara
        self.reittien_maara = reittien_maara
        self.reitit = []
        self.kaydyt_kaupungit = []  #Pitäisi uudelleen nimetä ryhmiksi tai joksikin muuksi
        self.oikea_reitti = []
        self.maaranpaa = None
        
    def add_reitti(self, reitti):
        """Lisää reitin reitit listaan"""
        self.reitit.append(reitti)
        
    def add_kaydyt_kaupungit(self, kaupunki):
        """Lisää kaupungin käytihin kaupunkeihin"""
        self.kaydyt_kaupungit.append(kaupunki)
        
    def add_oikea_reitti(self, reitti):
        """Lisää reitin oikea_reitti listaan"""
        self.oikea_reitti.append(reitti)
        
    def pop_reitti(self):
        """Ottaa seuraavan reitin listasta"""
        return self.reitit.pop()

class Solmu:  #Otettu mallia solmu esimerkistä
	def __init__(self,vier,nxt=None):
		self.viereinen = vier
        
def anna_korkeus(elem):
    """Antaa reitin korkeuden arvon"""
    return elem[2]
    #ei käytetä, koska en käytäkään sort()
        
def anna_kaupungit(elem):
    """Antaa reitin molempien kaupunkien arvot"""
    return elem[0], elem[1]
        
def onko_eri_ryhmassa(k1, k2, ryhmat):
    """Selvittää ovatko kaupungit erissä ryhmässä, ja jos ovat niin yhdistää niiden ryhmät"""
    k1_ryhma = -1 #Alustetaan niin, etteivät törmää vahingossakaan
    k2_ryhma = 1 + len(ryhmat)
    oliko = True
    for i in range(len(ryhmat)):  #katsoo jokaisen ryhmän
        #loytyy = 0;  #Nollataan aina muuttuja
        if k1_ryhma == k2_ryhma:   #Jos molemmat samassa, niin turha sitä on enää pyörittää 
            break
        for j in range(len(ryhmat[i])): #katsoo jokaisen ryhmän jäsenen
            if k1 == ryhmat[i][j]:
                k1_ryhma = i
            if k2 == ryhmat[i][j]:
                k2_ryhma = i
            if k1_ryhma == k2_ryhma: #Molemmat loytyivat saman ryhman sisältä
                oliko = False
                break
    if k1_ryhma != k2_ryhma:  #Jos molemmat eivät ole samassa ryhmässä
        ryhmat[k2_ryhma] += ryhmat[k1_ryhma] #Lisätään kaupunki ykkösen alkiot kaupungi kaksi listaan
        ryhmat.remove(ryhmat[k1_ryhma]) #poistetaan turha ryhmä
    return ryhmat, oliko
        
def kysy_tiedosto():
    """Kysyy käyttäjältä testattavan tiedoston, ei katso onko sisältö oikein, mutta katsoo onko olemassa"""
    try:
        tiedosto = open(input("\nAnna käytettävä tiedosto: "))
        return tiedosto
    except(IOError):
        print("Kohdetiedostoa ei voitu avata.")

def sulje_tiedosto(tiedosto):
    """Sulkee avoinna olevan tiedoston"""
    try:
        tiedosto.close()
    except(IOError):
        print("Kohdetiedostoa ei voitu sulkea.")
        
def muodosta_verkko(tiedosto):      #Jos tiedosto ei totauta oikeaa kaavaa, niin ei tietenkään toimi
    """Muodostaa verkon annatulla tiedostolla"""
    kaupunkien_maara, reittien_maara = (tiedosto.readline()).split(" ")             #Ottaa ylös kaupunkien määrän ja reittien määrän
    net = verkko(int(kaupunkien_maara), int(reittien_maara))                                                  #Muodostaa verkko luokan
    for i in range(int(reittien_maara)):
        reitti = [0,0,0]
        k1, k2, korkeus = (tiedosto.readline()).split(" ")                         #Ottaa ylös kaupungit ja korkeuden
        reitti[0] = int(k1)                                                                              #Ja laittaa ne listaan
        reitti[1] = int(k2)
        reitti[2] = int(korkeus)
        net.add_reitti(reitti)                                                                      #Laittaa reitin verkon reitteihin
    net.maaranpaa = int(tiedosto.readline())                                                        #Ottaa ylös määränpään + Tiedosto käyty Läpi
    return net                                                                                      #Palauttaa luodon verkon
    
def jarjesta_reitit(reitti_lista):
    """Järjestää reitit siten, että matalin reitti on viimeisenä"""
    #reitti_lista.sort(key=anna_korkeus)
    #reitti_lista.reverse()
    #Ylemmällä toimii nopeammin, mutta silloin en näytä että ymmärtäisin lajittelualgoritmeja
    listaa = []
    listaa = kekolajittelu(reitti_lista) 
    return listaa

#Nämä ovat muokattu HEAPSORTista, mutta minimi listan tekemiseen, eli suurimmat ensin ja pienimmät viimeisenä
def minimi_keko(lista,i,keon_koko):
    lft = 2*i+1
    rgt = 2*i+2
    smallest = 0
    if lft<keon_koko and lista[lft][2]<lista[i][2]:
        smallest = lft
    else:
        smallest = i

    if rgt<keon_koko and lista[rgt][2]<lista[smallest][2]:
        smallest = rgt
    
    if smallest != i:
        lista[i],lista[smallest] = lista[smallest],lista[i]
        minimi_keko(lista,smallest,keon_koko)

def rakenna_minimi_keko(lista,keon_koko):
    for k in range (int(keon_koko/2)-1,-1,-1):
        minimi_keko(lista,k,keon_koko)

def kekolajittelu (lista):
    keon_koko = len(lista)
    rakenna_minimi_keko(lista,keon_koko)
    for k in range(len(lista)-1,0,-1):
        lista[0],lista[k] = lista[k],lista[0]
        keon_koko -= 1
        minimi_keko(lista,0,keon_koko)
    return lista

def Algoritmi(verkko): #Pitäisi jakaa osiin, kuten leveys hakuun ja näin, mutta on kiireistä niin jätän tähän jamaan
    """Hiukan modifioitu Kruskalin algoritmi, muuten sama, mutta vielä lopussa katsoo parhaan reitin"""
    kaupunki = []
    loytyi_reitti = False
    
    seur_kaup = 1
    for rivi in range(verkko.kaupunkien_maara):
        verkko.kaydyt_kaupungit.append([])
        for sarake in range(1):
            verkko.kaydyt_kaupungit[-1].append(seur_kaup)
            seur_kaup += 1
    while True:
        if len(verkko.oikea_reitti) == (verkko.kaupunkien_maara - 1):
            loytyi_reitti = True
            break #Jos reittejä on yksi vähemmän kuin käytyjä kaupunkeja, niin silloin kaikki tarvittavat reitit on löydetty
        if len(verkko.reitit) == 0:
            break #Kaikki mahdolliset reitit käyty
        oliko = False
        reitti = verkko.pop_reitti()  #Ottaa seuraavan tutkittavan reitin
        kaupunki_1, kaupunki_2 = reitti[0], reitti[1] ##, reitti[2] , korkein_kohta
        verkko.kaydyt_kaupungit, oliko = onko_eri_ryhmassa(kaupunki_1, kaupunki_2, verkko.kaydyt_kaupungit)
        if oliko:
            verkko.oikea_reitti.append(reitti) #Lisätään reitit joissa kaupungit olivat aluksi erillään
    if not loytyi_reitti:
        return "Reittia ei loytynyt"
        
    solmuja = len(verkko.oikea_reitti)
    solmut = []
    vieruslista = []
    vari = []
    vanhempi = []
    queue = deque([])
    path = []
    
    for x in range(0, solmuja+2): #Tehdään lista, josta helmpompi käsitellä
        vieruslista.append([])
        vari.append(VALKOINEN)
        vanhempi.append(0)
        solmut.append(x)
    #Laitetaan solmut vieruslistoihin, eli molemmat kaupungit mukaan
    for k in range(0, solmuja):
        #verkko.oikea_reitti[k][0] tarkoittaa ensimmäistä kaupunkia ja jos 1 niin toista
        vieruslista[verkko.oikea_reitti[k][0]].append(Solmu(verkko.oikea_reitti[k][1])) #Laitetaan viereiset
        vieruslista[verkko.oikea_reitti[k][1]].append(Solmu(verkko.oikea_reitti[k][0]))
    
	vari[1] = HARMAA #Lähtee aina ykkösestä
	queue.append(1)
    
    #Aikalailla sama mitä esimerkissä
    while len(queue) != 0:
		u = queue.popleft()
		
		for reuna in vieruslista[u]:
			v = reuna.viereinen
			if vari[v] == VALKOINEN:
				vari[v] = HARMAA
				vanhempi[v] = u
				queue.append(v)	
		vari[u] = MUSTA
	u = verkko.maaranpaa
    while vanhempi[u] != 0:
        path.append(u)
        u = vanhempi[u]
    path.append(1)	
	path.reverse()
    
    #Olisi voinut tehdä varmasti järkevämmin, mutta en uskaltanut enää mennä sorkkimaan omaa koodiani
    korkein_kohta = 0
    for f in range(len(path)-1):
        #Katsotaan jokaisen oikeasti mukana olevan korkeus
        for t in range(solmuja):
            if (verkko.oikea_reitti[t][0] == path[f] and verkko.oikea_reitti[t][1] == path[f+1] and verkko.oikea_reitti[t][2] > korkein_kohta):
                korkein_kohta = verkko.oikea_reitti[t][2]
            if (verkko.oikea_reitti[t][0] == path[f+1] and verkko.oikea_reitti[t][1] == path[f] and verkko.oikea_reitti[t][2] > korkein_kohta):
                korkein_kohta = verkko.oikea_reitti[t][2]
    for z in range(len(path)):
        path[z] = str(path[z])
    sreitti = "->".join(path)
    tuloste = "Korkein kohta oli {}m.\nReitti oli:\n{}".format(str(korkein_kohta), sreitti)
    return tuloste
    
def main():
    tied = kysy_tiedosto()
    start = time.clock()
    verkot = muodosta_verkko(tied)
    sulje_tiedosto(tied)
    verkot.reitit = jarjesta_reitit(verkot.reitit)
    vastaus = Algoritmi(verkot)
    print("Kesti yhteensa: " + str(time.clock() - start) + " sekunttia\n")
    print(vastaus)
 
main()