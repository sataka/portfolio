♦ Group members:
    Santtu Käpylä - 2523734 - santtukaepylae@gmail.com
    Tiia Leinonen - 2551463 - tiia.leinonen@student.oulu.fi
    
♦ The name of the server where you tested your program
    haapa7: Host 185.38.3.233, TCP port 10000

♦ Step by step instructions on how to compile and run the program
    1. Go to server: haapa7
    2. Write on command line 
        python3 CourseWorkTemplate.py 185.38.3.233 10000 message,
        where the message is at least "HELLO", but if you want to you can add one or several of the following (order doesn't matter, use CAPS)
        MUL - multipart messages
        ENC - encrypting and decrypting of the messages
        PAR - parity
        example: "HELLO MUL ENC" NOTE! use quotation marks around the message, no need to add \r\n at the end of the message (implemented in code)
    3. Hit enter
    4. Enjoy
    

