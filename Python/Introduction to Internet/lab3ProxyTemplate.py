#!/usr/bin/python

import select
import socket
import string

'''
This is a template for lab3 exercise 2. It creates a server that listens for TCP and UDP connections and when it receives one it will execute either handle_tcp() or handle_udp() function. 
'''

# Hard coded values. You are allowed to hard code the target servers address if you like.
TCP_PORT = 21000
UDP_PORT = 21000
HOST = '' 
SERVER_ADRESS = '185.38.3.233'
SERVER_PORT = 20000

def handle_tcp(sock):
    '''
    This function should do the following:
    * When receiving a message from the client print the message content and somehow implicate where it came from. For example "Client sent X"
    * Create a TCP socket.
    * Forward the message to the server using the socket.
    * Print what you received from the server
    * Forward it to the client.
    * Close socket
    '''
    #sock is between client and proxy, s_s is between proxy and server
 
    enc_message = sock.recv(1500)   
    dec_message = enc_message.decode('utf-8')
    
    #print the message content
    print("Client sent: " + dec_message)
    
    #create a TCP socket
    s_s = socket.socket()
    
    #forward the message to the server using the socket
    s_s.sendall(enc_message)
    
    #print message received from server
    enc_message = s_s.recv(1500)
    dec_message = enc_message.decode('utf-8')
    print("Server sent: " + dec_message)
    
    #forward message to client
    sock.sendall(enc_message)
    
    #close socket
    s_s.close()  
    
    print("TCP happened")

    return


def handle_udp(sock):
    '''
    This function should do the following
    * When receiving a message from the client print the message content and somehow implicate where it came from. For example "Client sent X"
    * Create a UDP socket
    * Forward the message to the server using the socket.
    * A loop that does the following:
        * Print what you received from the server
        * Forward it to the client.
        * Break. DO NOT use message content as your break logic (if "QUIT" in message). Use socket timeout or some other mean.  
    * Close socket
    '''
    enc_message = sock.recvfrom(2048)
    dec_message = enc_message.decode('utf-8')
    #Print what client sent
    print("Client sent: " + dec_message)
    
    #Create UDP socket
    s_s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    
    #Forward the message to the server
    s_s.sendto(enc_message, (SERVER_ADRESS, SERVER_PORT))
    
    #loop
    while(True):
        enc_message = s_s.recvfrom(2048)
        dec_message = enc_message.decode('utf-8')
        
        #Print what server sent
        print("Server sent: " + dec_message)
        
        #Forward message to client
        sock.sendto(enc_message, (address, UDP_PORT))
        
        #Break
        if(sock.getdefaulttimeout() != None):
            break
            
    #close the socket
    s_s.close()
    
    print("UDP happened")
    return




def main():
    try:
        print("Creating sockets")
        '''
        Create and bind TCP and UDP sockets here. Use hard coded values TCP_PORT and UDP_PORT to choose your port
        Note that while loop below  uses these sockets, so name them tcp_socket and udp_socket or modify the loop below.
        '''
        tcp_socket = socket.socket()
        udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        tcp_socket.bind((HOST, TCP_PORT))
        ucp_socket.bind((HOST, UDP_PORT))
    except OSError:
        '''
    This will be raised if you are trying to create a socket but it is still active. Likely your code crashed or otherwise closed before you closed the socket. Wait a second and the socket should become available. Alternatively you can create a logic here that binds the socket to X_PORT+1. Doing this is not mandatory
        '''
        print("OSError was rised. Port is in use. Wait a second.")

    try:
        while True:
            i, o, e = select.select([tcp_socket, udp_socket], [], [], 5)
            if tcp_socket in i:
                handle_tcp(tcp_socket)
            if udp_socket in i:
                handle_udp(udp_socket)
    except NameError:
        print("Please create the sockets. NameError was raised doe to them missing.")
    finally:
        '''
        !!Close sockets here!!
        '''
        udp_socket.close()
        tcp_socket.close()
    


if __name__ == '__main__':
    main()
