/*
 *  ======== main.c ========
 */
/* XDCtools Header files */
#include <xdc/std.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <xdc/runtime/System.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Clock.h>

/* TI-RTOS Header files */
#include <ti/drivers/I2C.h>
#include <ti/drivers/PIN.h>
#include <ti/drivers/pin/PINCC26XX.h>
#include <ti/drivers/i2c/I2CCC26XX.h>
#include <ti/mw/display/Display.h>
#include <ti/mw/display/DisplayExt.h>


/* Board Header files */
#include "Board.h"

/* JTKJ Header files */
#include "wireless/comm_lib.h"
#include "sensors/bmp280.h"
#include "sensors/mpu9250.h"
#include "sensors/opt3001.h"

/* Task Stacks */
#define STACKSIZE 2048
Char displayTaskStack[STACKSIZE];
Char commTaskStack[STACKSIZE];


/* JTKJ: Display */
Display_Handle hDisplay;



/*Variables*/
char str[16];
char saatu[16] = "\"Tyhja\""; //Received message
uint16_t state = 0b000; //used for deterneming the state of the device //0b000 used instead of 0 just because I like it more
uint16_t menu_state = 0b000; //indicates where the pointer is on the menu
uint16_t msg_menu = 0b000;
uint16_t art_menu = 0b000;
bool measuring = 0;
uint16_t pisteet = 0;

int tulos;

uint16_t senderAddr;
uint8_t onko_viesti = 0;


bool vaihto = 0; //Shows when state is changed (Used to minimize to use of Display_clear())
int pituus = 0;
float taulukko[300][6];
float avg, keski, iavg;
double temp, pres, lux;

void laske_arvoja(float (*data)[6], int len, int paikka, float *avg, float *keski, float *iavg);
void tulokset(float avg, float keski, int *tulos);
void animaation();
void piirra_kuva(bool data[12][12]);

// Mustavalkoinen kuva: värit musta ja valkoinen
uint32_t imgPalette[] = {0, 0xFFFFFF};

const uint8_t imgData[8] = {
   0xFF, 
   0x81, 
   0x81, 
   0x81, 
   0x81, 
   0x81, 
   0x81, 
   0xFF
};

const uint8_t imgData2[8] = {
    0xFF, 
    0xFF,
    0xFF,
    0xFF,
    0xFF,
    0xFF,
    0xFF, 
    0xFF
};

const uint8_t imgData3[8] = {
    0x80, 
    0xC0,
    0xE0,
    0xF0,
    0xF8,
    0xFC,
    0xFE, 
    0xFF
};

const bool kuvaData[12][12] = {
    {0,0,0,0,0,0,0,0,0,0,0,0},
    {0,0,0,1,0,0,0,0,1,0,0,0},
    {0,0,1,1,1,0,0,1,1,1,0,0},
    {0,0,1,0,1,0,0,1,0,1,0,0},
    {0,0,1,1,1,0,0,1,1,1,0,0},
    {0,0,0,1,0,0,0,0,1,0,0,0},
    {0,1,0,0,0,0,0,0,0,0,1,0},
    {0,1,1,0,0,0,0,0,0,1,1,0},
    {0,1,1,1,0,0,0,0,1,1,1,0},
    {0,1,1,1,1,1,1,1,1,1,1,0},
    {0,0,1,1,1,1,1,1,1,1,0,0},
    {0,0,0,0,0,0,0,0,0,0,0,0}
    
};

// Kuvan määrittelyt
const tImage image = {
    .BPP = IMAGE_FMT_1BPP_UNCOMP,
    .NumColors = 2,
    .XSize = 1,
    .YSize = 8,
    .pPalette = imgPalette,
    .pPixel = imgData
};

const tImage image2 = {
    .BPP = IMAGE_FMT_1BPP_UNCOMP,
    .NumColors = 2,
    .XSize = 1,
    .YSize = 8,
    .pPalette = imgPalette,
    .pPixel = imgData2
};

const tImage image3 = {
    .BPP = IMAGE_FMT_1BPP_UNCOMP,
    .NumColors = 2,
    .XSize = 1,
    .YSize = 8,
    .pPalette = imgPalette,
    .pPixel = imgData3
};

// **
//
// MPU GLOBAL VARIABLES
//
// **
static PIN_Handle hMpuPin;
static PIN_State MpuPinState;
static PIN_Config MpuPinConfig[] = {
    Board_MPU_POWER  | PIN_GPIO_OUTPUT_EN | PIN_GPIO_HIGH | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    PIN_TERMINATE
};

// MPU9250 uses its own I2C interface
static const I2CCC26XX_I2CPinCfg i2cMPUCfg = {
    .pinSDA = Board_I2C0_SDA1,
    .pinSCL = Board_I2C0_SCL1
};

/* JTKJ: Pin Button1 configured as power button */
static PIN_Handle hButton1;
static PIN_State sButton1;
PIN_Config cButton1[] = {
    Board_BUTTON1 | PIN_INPUT_EN | PIN_PULLUP | PIN_IRQ_NEGEDGE,
    PIN_TERMINATE
};

PIN_Config cPowerWake[] = {
    Board_BUTTON1 | PIN_INPUT_EN | PIN_PULLUP | PINCC26XX_WAKEUP_NEGEDGE,
    PIN_TERMINATE
};

/* JTKJ: Pin Button0 configured as input */
static PIN_Handle hButton0;
static PIN_State sButton0;
PIN_Config cButton0[] = {
    Board_BUTTON0 | PIN_INPUT_EN | PIN_PULLUP | PIN_IRQ_NEGEDGE,
    PIN_TERMINATE
};

/* JTKJ: Leds */
static PIN_Handle hLed;
static PIN_State sLed;
PIN_Config cLed[] = {
    Board_LED0 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX, 
    PIN_TERMINATE
};

/* JTKJ: Handle for button1 (Is used for selecting/ backing from a state) */
Void Button1Fxn(PIN_Handle handle, PIN_Id pinId) {
    System_printf("Nappia 1 painettu: ");
    
    if(state == 0b000){ 
        if(menu_state == 0b100){
            vaihto = 0;
            menu_state = 0b101; 
            System_printf("Vaihdettu pointteri kohtaan %u\n", menu_state);
            System_flush();
        }
        else if(menu_state == 0b111){
            vaihto = 0;
            menu_state = 0b100; 
            System_printf("Vaihdettu pointteri kohtaan %u\n", menu_state);
            System_flush();
        }
        else{
            state = menu_state+1; //Sets the state to the state the pointer is pointing (aka menu_state)
            vaihto = 0;
            System_printf("Vaihdettu tilaan %u\n", state);
            System_flush();
        }
    }
    else if(state == 0b001 && measuring){
        laske_arvoja(taulukko, pituus, 3, &avg, &keski, &iavg);
        tulokset(avg, keski, &tulos);
        measuring = 0; //Stops the measuring
        pisteet = 0;
        state = 0b000; //Goes back to menu state 
        vaihto = 0;
        pituus = 0;
        System_printf("Mittaus lopetettu ja Vaihdettu tilaan %u\n", state);
        System_flush();
    }
    else if(state == 0b011){ //Viestintä tila
        if(msg_menu == 0b000){
            vaihto = 0;
            
            sprintf(str,"I am number 1!", 16); 
            Send6LoWPAN(0xFFFF, str, strlen(str));
            StartReceive6LoWPAN();
            System_printf("Viesti lähetetty\n");
            System_flush();
            msg_menu = 0b100;    //hoxhox
            
        }
        
        else if(msg_menu == 0b001){
            vaihto = 0;
            msg_menu = 0b100;
            if(tulos == 1){ //Portaat ylös
                sprintf(str,"1: Stairs up",16); 
                Send6LoWPAN(0xFFFF, str, strlen(str));    
            }
            else if(tulos == 2){ //Hissi ylös
                sprintf(str,"1: Elevator up",16); 
                Send6LoWPAN(0xFFFF, str, strlen(str));
            }
            else if(tulos == 3){ //Portaat alas
                sprintf(str,"1: Stairs down",16); 
                Send6LoWPAN(0xFFFF, str, strlen(str));    
            }
            else if(tulos == 4){ //Hissi alas
                sprintf(str,"1: Elevator down",16); 
                Send6LoWPAN(0xFFFF, str, strlen(str));
            }
            else{
                sprintf(str,"1: No data",16);
                Send6LoWPAN(0xFFFF, str, strlen(str));
            }
            StartReceive6LoWPAN();                                                                                   //!!!!!
            System_printf("Viesti lähetetty\n");
            System_flush();
        }
        else if(msg_menu == 0b010){
            vaihto = 0;
            msg_menu = 0b101;
            System_printf("Näytetään vastaanotettu viesti\n");
            PIN_setOutputValue( hLed, Board_LED0, 0); //Turns off the Led when the msg is read
            onko_viesti = 0;
            System_flush();
        }
        else if(msg_menu == 0b011){
            state = 0b000; //Goes back to menu state
            msg_menu = 0b000;
            vaihto = 0;
            System_printf("Vaihdettu tilaan %u\n", state);
            System_flush();
        }
        else if(msg_menu == 0b100){
            state = 0b000; //Goes back to menu state 
            msg_menu = 0b000;
            vaihto = 0;
            System_printf("Vaihdettu tilaan %u\n", state);
            System_flush();
        }
        else if(msg_menu == 0b101){
            msg_menu = 0b010;
            vaihto = 0;
            System_printf("Takaisin msg valikkoon\n");
            System_flush();
        }
    }
    else if(state == 0b111){ //Art? state
        if(art_menu == 0b000){ //Menu kohdalla Grafiikka
            vaihto = 0;
            art_menu = 0b100;
            System_printf("Menty Grafiikkaan/Kuvaan\n");
            System_flush();
        }
        else if(art_menu == 0b001){ //Menu kohdalla Animaation
            vaihto = 0;
            art_menu = 0b101;
            System_printf("Menty Animaatioon\n");
            System_flush();
        }
        else if(art_menu == 0b010){ //Menu kohdalla Runo
            vaihto = 0;
            art_menu = 0b110;
            System_printf("Menty Runoon\n");
            System_flush();
        }
        else if(art_menu == 0b011){ //Menu kohdalla Takaisin
            state = 0b000; //Goes back to menu state 
            art_menu = 0b000;
            vaihto = 0;
            System_printf("Vaihdettu tilaan %u\n", state);
            System_flush();
        }
        else if(art_menu == 0b100){ //Grafiikka
            vaihto = 0;
            art_menu = 0b000;
            System_printf("Takaisin Art menuun\n");
            System_flush();
        }
        else if(art_menu == 0b101){ //Animaatio
            vaihto = 0;
            art_menu = 0b001;
            System_printf("Takaisin Art menuun\n");
            System_flush();
        }
        else if(art_menu == 0b110){ //Runo
            vaihto = 0;
            art_menu = 0b010;
            System_printf("Takaisin Art menuun\n");
            System_flush();
        }
    }
    else{
        state = 0b000; //Goes back to menu state 
        vaihto = 0;
        System_printf("Vaihdettu tilaan %u\n", state);
        System_flush();
    }
}

void buttonFxn(PIN_Handle handle, PIN_Id pinId) {
    
    System_printf("Nappia 0 painettu: ");
    
    if(state == 0b000){ //Menu state
        if(menu_state == 0b100){ //CHANGE THIS VALUE IF YOU ARE ADDING NEW STATES!!!
            menu_state = 0b000; //When pointer is about to overflow it goes back to 0
            System_printf("Vaihdettu pointteri kohtaan %u\n", menu_state);
            System_flush();
        }
        else if(menu_state >= 0b111){
            menu_state = 0b101;
            System_printf("Vaihdettu pointteri kohtaan %u\n", menu_state);
            System_flush();
        }
        else{
            menu_state += 1; //Changes where menu pointer is pointing
            System_printf("Vaihdettu pointteri kohtaan %u\n", menu_state);
            System_flush();
        }
    }
    else if(state == 0b001){ //Measuring state
        if(!measuring){
            measuring = 1;
            vaihto = 0;
            System_printf("Mittaus aloitettu\n");
            System_flush();
        }
        else{
            laske_arvoja(taulukko, pituus, 3, &avg, &keski, &iavg);
            tulokset(avg, keski, &tulos);
            measuring = 0;
            pisteet = 0;
            vaihto = 0;
            pituus = 0;
            state = 0b010; //Goes to results state
            menu_state = 0b001; //When backing off results page your pointer points at results on the menu page
            System_printf("Mittaus lopetettu\n", menu_state);
            System_flush();
        }
    }
    else if(state == 0b011){ //Messaging state
        if(msg_menu == 0b100){
            state = 0b000; //Goes back to menu state 
            msg_menu = 0b000;
            vaihto = 0;
            System_printf("Vaihdettu tilaan %u\n", state);
            System_flush();
        }
        else if(msg_menu == 0b101){
            msg_menu = 0b010;
            vaihto = 0;
            System_printf("Takaisin msg valikkoon\n");
            System_flush();
        }
        else if(msg_menu >= 0b011){ //CHANGE THIS VALUE IF YOU ARE ADDING NEW ITEMS!!!
            msg_menu = 0b000; //When pointer is about to overflow it goes back to 0
            System_printf("Vaihdettu pointteri kohtaan %u\n", msg_menu);
            System_flush();
        }
        else{
            msg_menu += 1; //Changes where menu pointer is pointing
            System_printf("Vaihdettu pointteri kohtaan %u\n", msg_menu);
            System_flush();
        }
    }
    else if(state == 0b110){ // Sensors state
        state = 0b000; //Goes back to menu state 
        vaihto = 0;
        System_printf("Vaihdettu tilaan %u\n", state);
        System_flush();
    }
    else if(state == 0b111){ //Art state
        if(art_menu < 0b011){
            art_menu += 1;
            System_printf("Vaihdettu pointteri kohtaan %u\n", art_menu);
            System_flush();
        }
        else if(art_menu == 0b011){
            art_menu = 0b000;
            System_printf("Vaihdettu pointteri kohtaan %u\n", art_menu);
            System_flush();
        }
        else if(art_menu == 0b100){ //Grafiikka
            vaihto = 0;
            art_menu = 0b000;
            System_printf("Takaisin Art menuun\n");
            System_flush();
        }
        else if(art_menu == 0b101){ //Animaatio
            vaihto = 0;
            art_menu = 0b001;
            System_printf("Takaisin Art menuun\n");
            System_flush();
        }
        else if(art_menu == 0b110){ //Runo
            vaihto = 0;
            art_menu = 0b010;
            System_printf("Takaisin Art menuun\n");
            System_flush();
        }
    }
    else{
        System_printf("No effect\n");
        System_flush();
    }
}


void displayTask(UArg arg0, UArg arg1){ //Used to show relevant information on the screen 
    // setup here
    
    // JTKJ: Init Display
    Display_Params displayParams;
	displayParams.lineClearMode = DISPLAY_CLEAR_BOTH;
    Display_Params_init(&displayParams);
    
        
    // *******************************
    //
    // USE TWO DIFFERENT I2C INTERFACES
    //
    // *******************************

	I2C_Handle i2cMPU; // INTERFACE FOR MPU9250 SENSOR
	I2C_Params i2cMPUParams;
	I2C_Handle i2c; // INTERFACE FOR OTHER SENSORS
	I2C_Params i2cParams;
	
	I2C_Params_init(&i2cParams);
    i2cParams.bitRate = I2C_400kHz;

	float ax, ay, az, gx, gy, gz;
	//char str2[80];

    I2C_Params_init(&i2cMPUParams);
    i2cMPUParams.bitRate = I2C_400kHz;
    i2cMPUParams.custom = (uintptr_t)&i2cMPUCfg;

    // *******************************
    //
    // MPU OPEN I2C
    //
    // *******************************
    i2cMPU = I2C_open(Board_I2C, &i2cMPUParams);
    if (i2cMPU == NULL) {
        System_abort("Error Initializing I2CMPU\n");
    }

    // *******************************
    //
    // MPU POWER ON
    //
    // *******************************
    PIN_setOutputValue(hMpuPin,Board_MPU_POWER, Board_MPU_POWER_ON);

    // WAIT 100MS FOR THE SENSOR TO POWER UP
	Task_sleep(100000 / Clock_tickPeriod);
    System_printf("MPU9250: Power ON\n");
    System_flush();

    // *******************************
    //
    // MPU9250 SETUP AND CALIBRATION
    //
    // *******************************
	System_printf("MPU9250: Setup and calibration...\n");
	System_flush();

	mpu9250_setup(&i2cMPU);

	System_printf("MPU9250: Setup and calibration OK\n");
	System_flush();

    // *******************************
    //
    // MPU CLOSE I2C
    //
    // *******************************
    I2C_close(i2cMPU);
    
    // *******************************
    //
    // OTHER SENSOR OPEN I2C
    //
    // *******************************
    i2c = I2C_open(Board_I2C, &i2cParams);
    if (i2c == NULL) {
        System_abort("Error Initializing I2C\n");
    }

    //BMP280 SETUP
    bmp280_setup(&i2c);
    //I2C_close(i2c);
    
    opt3001_setup(&i2c);
    I2C_close(i2c);

    hDisplay = Display_open(Display_Type_LCD, &displayParams);
    if (hDisplay == NULL) {
        System_abort("Error initializing Display\n");
    }

    while(1){
        
        if(state == 0b000){ //Menu state
            if(!vaihto){
                Display_clear(hDisplay);
                vaihto = 1;
            }
            if(menu_state == 0b000){ //Puts the pointer on right place
                    Display_print0(hDisplay, 1, 1, "> Mittaa"); //Basic menu structure
                    Display_print0(hDisplay, 3, 1, "  Tulokset");
                    sprintf(str,"  Viestit(%u)", onko_viesti);
                    Display_print0(hDisplay, 5, 1, str);
                    Display_print0(hDisplay, 7, 1, "  Kiinni");
                    Display_print0(hDisplay, 9, 1, "  Extrat");
            }
            else if(menu_state == 0b001){
                    Display_print0(hDisplay, 1, 1, "  Mittaa"); 
                    Display_print0(hDisplay, 3, 1, "> Tulokset"); //Need all lines because if coming back from another task, some of the lines would not be there after clear.
                    sprintf(str,"  Viestit(%u)", onko_viesti);
                    Display_print0(hDisplay, 5, 1, str);
                    Display_print0(hDisplay, 7, 1, "  Kiinni");
                    Display_print0(hDisplay, 9, 1, "  Extrat");
            }
            else if(menu_state == 0b010){ 
                    Display_print0(hDisplay, 1, 1, "  Mittaa"); 
                    Display_print0(hDisplay, 3, 1, "  Tulokset");
                    sprintf(str,"> Viestit(%u)", onko_viesti);
                    Display_print0(hDisplay, 5, 1, str);
                    Display_print0(hDisplay, 7, 1, "  Kiinni");
                    Display_print0(hDisplay, 9, 1, "  Extrat");
            }
            else if(menu_state == 0b011){ 
                    Display_print0(hDisplay, 1, 1, "  Mittaa"); 
                    Display_print0(hDisplay, 3, 1, "  Tulokset");
                    sprintf(str,"  Viestit(%u)", onko_viesti);
                    Display_print0(hDisplay, 5, 1, str);
                    Display_print0(hDisplay, 7, 1, "> Kiinni");
                    Display_print0(hDisplay, 9, 1, "  Extrat");
            }
            else if(menu_state == 0b100){ 
                    Display_print0(hDisplay, 1, 1, "  Mittaa"); 
                    Display_print0(hDisplay, 3, 1, "  Tulokset");
                    sprintf(str,"  Viestit(%u)", onko_viesti);
                    Display_print0(hDisplay, 5, 1, str);
                    Display_print0(hDisplay, 7, 1, "  Kiinni");
                    Display_print0(hDisplay, 9, 1, "> Extrat");
            }
            else if(menu_state == 0b101){ 
                    Display_print0(hDisplay, 1, 1, "> Sensorit"); 
                    Display_print0(hDisplay, 3, 1, "  Taide");
                    Display_print0(hDisplay, 5, 1, "  TAKAISIN");
            }
            else if(menu_state == 0b110){ 
                    Display_print0(hDisplay, 1, 1, "  Sensorit"); 
                    Display_print0(hDisplay, 3, 1, "> Taide");
                    Display_print0(hDisplay, 5, 1, "  TAKAISIN");
            }
            else if(menu_state == 0b111){ 
                    Display_print0(hDisplay, 1, 1, "  Sensorit"); 
                    Display_print0(hDisplay, 3, 1, "  Taide");
                    Display_print0(hDisplay, 5, 1, "> TAKAISIN");
            }
            Task_sleep(50000 / Clock_tickPeriod);
        }
        
        else if(state == 0b001){ //Measure state 
            
            if(!vaihto){
                Display_clear(hDisplay);
                vaihto = 1;
            }
            
            if(!measuring){
                Display_print0(hDisplay, 1, 1, "Aloita mittaus"); //Helpot ohjeet
                Display_print0(hDisplay, 3, 1, "painamalla B0.");
                Task_sleep(100000 / Clock_tickPeriod);
            }
            else if(measuring){ 
                
                i2cMPU = I2C_open(Board_I2C, &i2cMPUParams);
                if (i2cMPU == NULL) {
                    System_abort("Error Initializing I2CMPU\n");
                }
                
                if(pisteet >= 8){
                    pisteet = 0;
                }
                
                if(!vaihto){
                    Display_clear(hDisplay);
                    vaihto = 1;
                }
                
                if(pisteet == 0){
                    Display_print0(hDisplay, 1, 1, "Mitataan");
                    Display_print0(hDisplay, 5, 1, "Lopeta mittaus");
                    Display_print0(hDisplay, 7, 1, "painamalla B0.");
                }
                else if(pisteet == 2){
                    Display_print0(hDisplay, 1, 1, "Mitataan.");
                }
                else if(pisteet == 4){
                    Display_print0(hDisplay, 1, 1, "Mitataan..");
                }
                else if(pisteet == 6){
                    Display_print0(hDisplay, 1, 1, "Mitataan...");
                }

                mpu9250_get_data(&i2cMPU, &ax, &ay, &az, &gx, &gy, &gz);
                System_flush();
                
                taulukko[pituus][0] = ax;
                taulukko[pituus][1] = ay;
                taulukko[pituus][2] = az;
                taulukko[pituus][3] = gx;
                taulukko[pituus][4] = gy;
                taulukko[pituus][5] = gz;
                
                pituus += 1;
                pisteet += 1;
                
                if(pituus >= 301){
                    laske_arvoja(taulukko, pituus, 3, &avg, &keski, &iavg);
                    tulokset(avg, keski, &tulos);
                    measuring = 0;
                    pisteet = 0;
                    vaihto = 0;
                    pituus = 0;
                    state = 0b010; //Goes to results state
                    menu_state = 0b001; //When backing off results page your pointer points at results on the menu page
                    System_printf("Mittaus lopetettu; Taulukko täynnä\n");
                    System_flush();
                }
                I2C_close(i2cMPU);

                Task_sleep(25000 / Clock_tickPeriod);
            }
            
        }
        
        else if(state == 0b010){ // Result state
            
            if(!vaihto){
                Display_clear(hDisplay);
                vaihto = 1;
            }
            Display_print0(hDisplay, 1, 1, "Tulokset:");
            sprintf(str,"AVG: %f", avg);
            Display_print0(hDisplay, 3, 1, str);
            sprintf(str,"KH: %f", keski);
            Display_print0(hDisplay, 5, 1, str);
            if(tulos == 1){
                Display_print0(hDisplay, 7, 1, "Portaat ylos");
            }
            else if(tulos == 2){
                Display_print0(hDisplay, 7, 1, "Hissi ylos");
            }
            else if(tulos == 3){
                Display_print0(hDisplay, 7, 1, "Portaat alas");
            }
            else if(tulos == 4){
                Display_print0(hDisplay, 7, 1, "Hissi alas");
            }
            else{
                Display_print0(hDisplay, 7, 1, "Ei dataa");
            }
            
            
        }
        else if(state == 0b011){ // Messaging state
            if(!vaihto){
                Display_clear(hDisplay);
                vaihto = 1;
            }
            if(msg_menu == 0b000){ //Puts the pointer on right place
                    Display_print0(hDisplay, 1, 1, "Laheta:"); 
                    Display_print0(hDisplay, 3, 1, "> \"Terveisia\"");
                    Display_print0(hDisplay, 5, 1, "  \"Tulokset\"");
                    sprintf(str,"  INBOX(%u)", onko_viesti);
                    Display_print0(hDisplay, 7, 1, str);
                    Display_print0(hDisplay, 9, 1, "  TAKAISIN");
            }
            else if(msg_menu == 0b001){
                    Display_print0(hDisplay, 1, 1, "Laheta:"); 
                    Display_print0(hDisplay, 3, 1, "  \"Terveisia\"");
                    Display_print0(hDisplay, 5, 1, "> \"Tulokset\"");
                    sprintf(str,"  INBOX(%u)", onko_viesti);
                    Display_print0(hDisplay, 7, 1, str);
                    Display_print0(hDisplay, 9, 1, "  TAKAISIN");
            }
            else if(msg_menu == 0b010){ 
                    Display_print0(hDisplay, 1, 1, "Laheta:"); 
                    Display_print0(hDisplay, 3, 1, "  \"Terveisia\"");
                    Display_print0(hDisplay, 5, 1, "  \"Tulokset\"");
                    sprintf(str,"> INBOX(%u)", onko_viesti);
                    Display_print0(hDisplay, 7, 1, str);
                    Display_print0(hDisplay, 9, 1, "  TAKAISIN");
            }
            else if(msg_menu == 0b011){ 
                    Display_print0(hDisplay, 1, 1, "Laheta:"); 
                    Display_print0(hDisplay, 3, 1, "  \"Terveisia\"");
                    Display_print0(hDisplay, 5, 1, "  \"Tulokset\"");
                    sprintf(str,"  INBOX(%u)", onko_viesti);
                    Display_print0(hDisplay, 7, 1, str);
                    Display_print0(hDisplay, 9, 1, "> TAKAISIN");
            }
            else if(msg_menu == 0b100){
                Display_print0(hDisplay, 5, 3, "LAHETETTY");
            }
            else if(msg_menu == 0b101){
                sprintf(str,"%s", saatu);
                Display_print0(hDisplay, 1, 0, "Saadut viestit:"); //Max 1 viesti näkyy, mutta kuulostaa paremmalta kuin saatu viesti 
                Display_print0(hDisplay, 3, 0, str);
                Display_print0(hDisplay, 5, 0, "Press any button");
                Display_print0(hDisplay, 7, 0, "to continue");
                
            }
            Task_sleep(50000 / Clock_tickPeriod);
        }
        else if(state == 0b100){ // Turn off the device
            Display_clear(hDisplay);
            Display_print0(hDisplay, 5, 2, "SAMMUTETAAN");
            System_printf("Kone kiinni\n");
            System_flush();
            
            Display_clear(hDisplay);
            Display_close(hDisplay);
            Task_sleep(1000000 / Clock_tickPeriod);
    
            PIN_close(hButton1);
    
            PINCC26XX_setWakeup(cPowerWake);
            Power_shutdown(NULL,0);
        }
        else if(state == 0b110){ //Sensors Task
        
            if(!vaihto){
                Display_clear(hDisplay);
                vaihto = 1;
            }
            i2c = I2C_open(Board_I2C, &i2cParams);
            if (i2c == NULL) {
                System_abort("Error Initializing I2C\n");
            }
            
            bmp280_get_data(&i2c, &pres, &temp);
            lux = opt3001_get_data(&i2c);
            I2C_close(i2c);
            
            sprintf(str,"Temp: %4.2f C", temp);
            Display_print0(hDisplay, 1, 0, str);
            sprintf(str,"Pres: %4.1f KPa", pres);
            Display_print0(hDisplay, 3, 0, str);
            sprintf(str,"Light: %5.2f lx", lux);
            Display_print0(hDisplay, 5, 0, str);
            Display_print0(hDisplay, 9, 1, "> TAKAISIN");
            Task_sleep(1000000 / Clock_tickPeriod);
            
        }
        
        else if(state == 0b111){ //Art Task
        
            if(!vaihto){
                Display_clear(hDisplay);
                vaihto = 1;
            }
            
            if(art_menu == 0b000){
                    Display_print0(hDisplay, 1, 1, "> Kuva"); 
                    Display_print0(hDisplay, 3, 1, "  Animaatio");
                    Display_print0(hDisplay, 5, 1, "  Runo");
                    Display_print0(hDisplay, 7, 1, "  TAKAISIN");
            }
            else if(art_menu == 0b001){
                    Display_print0(hDisplay, 1, 1, "  Kuva"); 
                    Display_print0(hDisplay, 3, 1, "> Animaatio");
                    Display_print0(hDisplay, 5, 1, "  Runo");
                    Display_print0(hDisplay, 7, 1, "  TAKAISIN");
            }
            else if(art_menu == 0b010){
                    Display_print0(hDisplay, 1, 1, "  Kuva"); 
                    Display_print0(hDisplay, 3, 1, "  Animaatio");
                    Display_print0(hDisplay, 5, 1, "> Runo");
                    Display_print0(hDisplay, 7, 1, "  TAKAISIN");
            }
            else if(art_menu == 0b011){
                    Display_print0(hDisplay, 1, 1, "  Kuva"); 
                    Display_print0(hDisplay, 3, 1, "  Animaatio");
                    Display_print0(hDisplay, 5, 1, "  Runo");
                    Display_print0(hDisplay, 7, 1, "> TAKAISIN");  
                
            }
            else if(art_menu == 0b100){ //Grafiikka
                piirra_kuva(kuvaData);
            }
            else if(art_menu == 0b101){ // Animaation
                animaatio();
            }
            else if(art_menu == 0b110){ // Runo
                    Display_print0(hDisplay, 3, 1, "Aasi svengasi,"); 
                    Display_print0(hDisplay, 5, 1, "yksin hengasi.");
            }
        }

        Task_sleep(100000 / Clock_tickPeriod);
    }
    
}

void animaatio(){
    tContext *pContext = DisplayExt_getGrlibContext(hDisplay);
	int hor, ver, kert, arvo;
	while(1){
	    if(!vaihto){
            break;
	    }
	    
        for(hor=0; hor< 12;hor++){
            for(ver=0; ver< 12;ver++){
                if(hor < ver){
                    if(!vaihto){break;}
                    GrImageDraw(pContext, &image, hor*8, ver*8); 
                }
                if(!vaihto){break;}
                GrImageDraw(pContext, &image3, hor*8, hor*8);
            }
        }
        GrFlush(pContext);
        
        for(kert=0;kert<11;kert++){
            for(arvo=0;arvo<11;arvo++){
                if(!vaihto){break;}
                if(arvo > kert){
                    break;
                }
                else{
                    GrImageDraw(pContext, &image2, kert*8-arvo*8, 88-arvo*8);
                }
            }
            if(!vaihto){break;}
            GrFlush(pContext);
            Task_sleep(100000 / Clock_tickPeriod);
        }
        
        for(kert=0;kert<11;kert++){
            for(arvo=0;arvo<11;arvo++){
                if(!vaihto){break;}
                if(arvo > kert){
                    break;
                }
                else{
                    GrImageDraw(pContext, &image3, 88-arvo*8, kert*8-arvo*8);
                }
            }
            
            if(!vaihto){break;}
	        
            GrFlush(pContext);
            Task_sleep(100000 / Clock_tickPeriod);
        }
        
        for(kert=11;kert>=0;kert--){
            for(arvo=11;arvo>=0;arvo--){
                if(!vaihto){break;}
                GrImageDraw(pContext, &image2, 88-arvo*8, kert*8-arvo*8);
            }
            if(!vaihto){break;}
            
            GrFlush(pContext);
            Task_sleep(100000 / Clock_tickPeriod);
        }
        if(!vaihto){break;}
        Task_sleep(100000 / Clock_tickPeriod);
        Display_clear(hDisplay);
	}
}

void tulokset(float avg, float keski, int *tulos){  //iavg = itseisarvoilla laskettu xyz keskiarvo
    
    if(keski >= 25){                      
        if(avg >= 0){ //Portaat ylös
            *tulos = 1;
        }
        else if(avg < 0){ //Portaat alas
            *tulos = 3;
        }
    }
    else if(keski < 25){
        if(avg >= 0){ //Hissi alas
            *tulos = 4;
        }
        else if(avg < 0){ //Hissi Ylös
            *tulos = 2;
        }
    }
}

void laske_arvoja(float (*data)[6], int len, int paikka, float *avg, float *keski, float *iavg) {
    int i;
    float kavg, ikavg, keskihajonta = 0.0, summa = 0.0, isumma = 0.0;
    
    for(i=0;i<len;i++) {
        isumma += abs(data[i][paikka]) + abs(data[i][paikka+1]) + abs(data[i][paikka+2]);  
    }
    
    for(i=0;i<len;i++) {
        summa += data[i][paikka] + data[i][paikka+1] + data[i][paikka+2]; 
    }
    
    ikavg = isumma/len;
    kavg = summa/len;
    
    *avg = kavg;
    *iavg = ikavg;

    for(i=0; i<len; ++i) {
        keskihajonta += pow((data[i][paikka] + data[i][paikka+1] + data[i][paikka+2]) - kavg, 2);
    }

    *keski = sqrt(keskihajonta/len);
}

void piirra_kuva(bool data[12][12]){
    tContext *pContext = DisplayExt_getGrlibContext(hDisplay);
    int hor, ver;
    for(hor=0; hor < 12; hor++){
        for(ver=0; ver < 12; ver++){
            if(!data[ver][hor]){
                GrImageDraw(pContext, &image2, hor*8, ver*8);
            }
        }
    }
    GrFlush(pContext);
}

Void commTask(UArg arg0, UArg arg1) {
    
    //char Payload[16];
    uint16_t senderAddr;
    // Radio to receive mode
	int32_t result = StartReceive6LoWPAN();
	if(result != true) {
		System_abort("Wireless receive mode failed");
	}

    while (1) {

        // DO __NOT__ PUT YOUR SEND MESSAGE FUNCTION CALL HERE!! 

    	// NOTE THAT COMMUNICATION WHILE LOOP DOES NOT NEED Task_sleep
    	// It has lower priority than main loop (set by course staff)
        if (GetRXFlag()) {

             // Tyhjennetään puskuri (ettei sinne jäänyt edellisen viestin jämiä)
            memset(saatu,0,16);
            // Luetaan viesti puskuriin payload
           Receive6LoWPAN(&senderAddr, saatu, 16);
           // Tulostetaan vastaanotettu viesti konsoli-ikkunaan
           onko_viesti = 1;
           PIN_setOutputValue( hLed, Board_LED0, 1); //Turn on Led to notify on msg
           System_printf(saatu);
           System_flush();
           
      } 
      //Task_sleep(50000/Clock_tickPeriod);
    }
}



Int main(void) {
    
    Task_Handle hDisplayTask;
    Task_Params displayTaskParams;
    
    /*Task_Handle hSensorFxn;
    Task_Params sensorFxnParams;*/
        
    Task_Handle hCommTask;
    Task_Params commTaskParams;

    // Initialize board
    Board_initGeneral();
    Board_initI2C();

    hMpuPin = PIN_open(&MpuPinState, MpuPinConfig);
    if (hMpuPin == NULL) {
        System_abort("Pin open failed!");
    }

	/* JTKJ: Button1 */
	hButton1 = PIN_open(&sButton1, cButton1);
	if(!hButton1) {
		System_abort("Error initializing button1 shut pins\n");
	}
	if (PIN_registerIntCb(hButton1, &Button1Fxn) != 0) {
		System_abort("Error registering button1 callback function");
	}
    /* JTKJ: Button0 */
    hButton0 = PIN_open(&sButton0, cButton0);
    if( !hButton0 ) {
      System_abort("Error initializing button0 shut pins\n");
    }
    if (PIN_registerIntCb(hButton0, &buttonFxn) != 0) {
      System_abort("Error registering button callback function");
    }


    /* JTKJ: Init Leds */  //NOT IN USE RIGHT NOW
   hLed = PIN_open(&sLed, cLed);
    if(!hLed) {
        System_abort("Error initializing LED pin\n");
    }
    
    
    Task_Params_init(&displayTaskParams);
    displayTaskParams.stackSize = STACKSIZE;
    displayTaskParams.stack = &displayTaskStack;
    displayTaskParams.priority=2;

    hDisplayTask = Task_create(displayTask, &displayTaskParams, NULL);
    if (hDisplayTask == NULL) {
    	System_abort("Task create failed!");
    }
    
    /*Task_Params_init(&sensorFxnParams);
    sensorFxnParams.stackSize = STACKSIZE;
    sensorFxnParams.stack = &sensorFxnStack;
    sensorFxnParams.priority=2;

    hSensorFxn = Task_create(sensorFxn, &sensorFxnParams, NULL);
    if (hSensorFxn == NULL) {
    	System_abort("Task create failed!");
    }*/

    /* JTKJ: Init Communication Task */
    Init6LoWPAN();
    
    Task_Params_init(&commTaskParams);
    commTaskParams.stackSize = STACKSIZE;
    commTaskParams.stack = &commTaskStack;
    commTaskParams.priority=1;
    
    hCommTask = Task_create(commTask, &commTaskParams, NULL);
    if (hCommTask == NULL) {
    	System_abort("Task create failed!");
    }

    // JTKJ: Send hello to console
    System_printf("Kanaa ja Kriisiä\n");
    System_flush();

    /* Start BIOS */
    BIOS_start();

    return (0);
}

