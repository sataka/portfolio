LIBRARY ieee;
USE ieee.std_logic_1164.all; 
USE ieee.numeric_std.all;

Entity REGnp is
generic (wid : natural := 8);
port (
	clk, resetn, load, enable: in std_logic;
	input 	:	in std_logic_vector(wid-1 downto 0);
	output	:	out std_logic_vector(wid-1 downto 0));
end entity REGnp;
--
architecture RTL of REGnp is
	
begin
process (clk, resetn, load, enable) is
	begin
		if resetn = '0' then
			output <= (others=>'1');				-- all bits zeroed
		elsif rising_edge(clk) and load = '1' and enable = '1' then
			output <= input;
		end if;
end process;
		
end architecture RTL;