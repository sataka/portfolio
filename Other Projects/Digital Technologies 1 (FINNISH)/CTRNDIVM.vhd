LIBRARY ieee;
USE ieee.std_logic_1164.all; 
USE ieee.numeric_std.all;
use IEEE.MATH_REAL.all;

Entity CTRNDIVM is

generic (
	N : natural := 8;									-- number of bits
	M : natural := 255								-- repetition rate
	);

port (
	clk, resetn, en_count, sync_reset: in std_logic;
	tc: out std_logic;								-- overflow flag
	ct: out std_logic_vector(N - 1 downto 0)	-- output bits
	);	
	
end entity CTRNDIVM;
--
architecture RTL of CTRNDIVM is
	signal count: unsigned(N - 1 downto 0);	-- counter bits
begin
process (clk, resetn, en_count, sync_reset) is
	begin
		if resetn = '0' then
			count <= (others=>'0');				-- all bits zeroed
		elsif rising_edge(clk) then
				if sync_reset = '1' then		-- synchronoous reset
						count <= (others=>'0');
					elsif count < M-1 and en_count = '1' then			-- normal counting
						count <= count + 1;
					elsif en_count = '1' then  
					count <= (others=>'0');		-- overflow
				end if;
		end if;
end process;

process (count, en_count) is
	begin
		if (count = M-1 and en_count = '1') then	-- overflow?
			tc <= '1';										-- yes
		else
			tc <= '0';										-- no
		end if;
		ct <= STD_LOGIC_VECTOR(count);
end process;
		
end architecture RTL;