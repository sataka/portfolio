LIBRARY ieee;
USE ieee.std_logic_1164.all; 

ENTITY burst_control IS
	PORT
	(
		clk							: IN	STD_LOGIC;
		resetn						: IN	STD_LOGIC;
		mode							: IN	STD_LOGIC;
		p								: IN	STD_LOGIC;
		n								: IN	STD_LOGIC;
		s_out							: OUT STD_LOGIC_VECTOR(2 downto 0);
		p_out							: OUT	STD_LOGIC
		);
END ENTITY burst_control;

ARCHITECTURE RTL OF burst_control IS
	TYPE STATE_TYPE IS (idle, enable, stop);
	SIGNAL state, next_state: STATE_TYPE;
	signal p_in : std_logic;
BEGIN
	PROCESS (clk, resetn,p_in) IS
	BEGIN
		IF resetn = '0' THEN
			state <= idle;
		ELSIF rising_edge(clk) THEN
			state <= next_state;
			p_out <= p_in;
		END IF;
	END PROCESS;

	PROCESS (state,mode,p,n) IS
	BEGIN
		CASE state IS
			WHEN idle =>
				IF n = '1' and mode = '0' THEN
					next_state <= enable;
				ELSE
					next_state <= idle;
				END IF;
			p_in <= p and mode;
			s_out <= "001";

			WHEN enable =>
				IF p = '1' and mode = '0' THEN
					next_state <= stop;
				ELSE
					next_state <= enable;
				END IF;
			p_in <= p;
			s_out <= "010";


			WHEN stop =>
				IF p = '1' and mode = '0' THEN
					next_state <= idle;
				ELSE
					next_state <= stop;
				END IF;
			p_in <= p;
			s_out <= "100";

		END CASE;
	END PROCESS;
		
END ARCHITECTURE RTL;

