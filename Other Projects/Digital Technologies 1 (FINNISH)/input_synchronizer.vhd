-- input signal synchronizer with pulse outputs for rising and falling edges
library ieee;
use ieee.std_logic_1164.all;

entity input_synchronizer is

	port 
	(
		clk			: in std_logic;	
		input			: in std_logic;	
		resetn		: in std_logic;	
		pulse_out_r	: out std_logic;	
		pulse_out_f	: out std_logic	
	);

end entity;

architecture rtl of input_synchronizer is

	signal sr: std_logic_vector(2 downto 0);

begin

	process (clk, resetn, input)
	begin
		if (resetn = '0') then
			
				sr <= (others=>'0');
				pulse_out_r <= '0';
				pulse_out_f <= '0';
			
		elsif (rising_edge(clk)) then
			sr <= sr(1 downto 0) & input;			
			pulse_out_r <= sr(1) and not sr(2);
			pulse_out_f <= sr(2) and not sr(1);
		end if;
	end process;

	end rtl;
