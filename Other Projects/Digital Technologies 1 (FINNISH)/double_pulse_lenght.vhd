LIBRARY ieee;
USE ieee.std_logic_1164.all; 

ENTITY double_pulse_lenght IS
	PORT
	(
		clk							: IN	STD_LOGIC;
		resetn						: IN	STD_LOGIC;
		p								: IN	STD_LOGIC;
		p_out							: OUT	STD_LOGIC
		);
END ENTITY double_pulse_lenght;

ARCHITECTURE RTL OF double_pulse_lenght IS
	TYPE STATE_TYPE IS (idle, one, two);
	SIGNAL state, next_state: STATE_TYPE;
	signal p_in : std_logic;
BEGIN
	PROCESS (clk, resetn,p_in) IS
	BEGIN
		IF resetn = '0' THEN
			state <= idle;
		ELSIF rising_edge(clk) THEN
			state <= next_state;
			p_out <= p_in;
		END IF;
	END PROCESS;

	PROCESS (state,p) IS
	BEGIN
		CASE state IS
			WHEN idle =>
				IF p = '1' THEN
					next_state <= one;
				ELSE
					next_state <= idle;
				END IF;
			p_in <= '0';

			WHEN one =>
					next_state <= two;
			p_in <= '1';

			WHEN two =>
					next_state <= idle;
			p_in <= '1';

		END CASE;
	END PROCESS;
		
END ARCHITECTURE RTL;

