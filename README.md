# Portfolio

This repository contains school projects I have done from 2016 onwards. Some of the projects have been done in groups, and some of them are in Finnish. This portfolio was done with haste, so some projects miss some files, and there are no good explanations for what every code should do, and everything is its original form.

The projects done in Finnish are marked with (FINNISH).

Sorry for inconvinience, and messiness.

Santtu Käpylä - 28.01.2020

